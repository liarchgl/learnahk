; send j when mouse click down
While(1)
{
    state := GetKeyState("LButton" , "P")
    ; state := GetKeyState("j" , "P")
    if(state >= 1)
    {
        Send, j
        ; MsgBox, click
        Click, down
    }
    Sleep, 10
}

; Untitled - Notepad
; #IfWinActive 无标题 - 记事本
; MsgBox, Wow!
; Run, notepad.exe

; #IfWinActive Untitled - Notepad

; Esc::
; MsgBox, Escape!!!!
; return

; :*:btw::
; MsgBox, You typed btw.
; return

; ^j::
; MsgBox, Wow!
; MsgBox, There are
; Run, notepad.exe
; WinActivate, Untitled - Notepad
; WinWaitActive, Untitled - Notepad
; Send, 7 lines{!}{Enter}
; SendInput, inside the CTRL{+}J hotkey.
; return

; Numpad0 & Numpad1::
; MsgBox, You pressed Numpad1 while holding down Numpad0.
; return

; Numpad0 & Numpad2::
; Run, notepad.exe
; return

; !q::
; MsgBox, You pressed ALT+Q in Notepad.
; return

; ; Any window that isn't Untitled - Notepad
; ; #IfWinActive
; ^q::
; MsgBox, You pressed CTRL+Q in any window.

; #i::
; Run, http://www.google.com/
; return

; ^p::
; Run, notepad.exe
; return

; :*:j::
; Send, jack
; return

; ^k::
; Send, {Up down}
; Sleep, 3000
; Send, {Up up}

; ; run any file you want, it will automatically use the default program if you run a file not a exe
; ^k::
; Run, C:\Users\liarc\Desktop\Short\Excel


; :*:acheiv::achiev
; ::achievment::achievement
; ::acquaintence::acquaintance
; :*:adquir::acquir
; ::aquisition::acquisition
; :*:agravat::aggravat
; :*:allign::align
; ::ameria::America

; test while loop and assign
; ^k::
; x := 20
; ans := 0
; While, x >= 0
; {
;     x := x-1
;     ans := ans + 1
; }
; Send, %ans%

; test inputbox
; ^j::
; InputBox, OutputVar, Question 1, What is your first name?
; if (OutputVar = "Bill")
;     MsgBox, That's an awesome name`, %OutputVar%.
; return

; ^k::
; InputBox, OutputVar2, Question 2, Do you like AutoHotkey?
; if (OutputVar2 = "yes")
;     MsgBox, Thank you for answering %OutputVar2%`, %OutputVar%! We will become great friends.
; else
;     MsgBox, %OutputVar%`, That makes me sad.
; return

; reload
^l::
    MsgBox,4,, Reload script?
    IfMsgBox, Yes
    {
        Reload
    }
    return

^p::
    Pause
    return

; test choose window
; ^j::
; MsgBox, 4, , like it?, 5
; IfMsgBox, Yes
;     MsgBox, good1
; IfMsgBox, No
;     MsgBox, Sad
; IfMsgBox, Timeout
;     MsgBox, why
; IfMsgBox, Cancel
;     MsgBox, please
; return

; test object delete, create, use
; ^j::
; mo := {33:"oo", "S":"ss", "3s":83}
; ar := ["fdsa", "sss", 399]
; inda = 1
; indb = 2
; x := mo.Delete(33)
; MsgBox, %x%
; x := mo[33]
; MsgBox, %x%
; return

; test window
; ^j::
; if (not WinExist("ahk_class Notepad"))
; {
;     Run Notepad
;     WinWait ahk_class Notepad
; }
; else
;     WinActivate ahk_class Notepad
; Send Hello`, world!{Enter}
; return

; file := ""
; ^j::
;     if(file == "")
;     {
;         file := FileOpen("C:\Users\liarc\Desktop\AHKScriptsSamples\words.txt", "rw")
;     }
;     ; text0 = asdfFF
;     ; FileAppend, %text0%, C:\Users\liarc\Desktop\AHKScriptsSamples\words.txt
;     ; FileRead, OutputVar, C:\Users\liarc\Desktop\AHKScriptsSamples\words.txt
;     file.Pos := 0
;     file.Encoding := "UTF-8"
;     out := file.ReadInt()
;     Send, This file's first line is %out%.{Enter}
;     out := file.ReadInt()
;     Send, This file's first line is %out%.{Enter}
;     out := file.ReadInt()
;     Send, This file's first line is %out%.{Enter}
;     out := file.Length
;     Send, This file's length is %out%.{Enter}
;     out := file.Encoding
;     Send, This file's encoding is %out%.{Enter}
;     return

; get window title
; ^g::
;     WinGetActiveTitle, wname 
;     MsgBox, %wname%
;     return
