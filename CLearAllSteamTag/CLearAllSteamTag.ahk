﻿^j::
    SteamWinTitle = Steam
    uid := LSChangeToWindow(steamWinTitle)
    if(uid)
    {

    }
    else
    {
        MsgBox, Please open steam window.
        return
    }

    WinGetPos, , , SteamWidth, SteamHeight, %SteamWinTitle%

    ; change to steam libary
    ImageSearch, OutputVarX, OutputVarY, 0, 0, %SteamWidth%, %SteamHeight%, steam_ku.png
    if(OutputVarX)
    {
        Click, %OutputVarX%, %OutputVarY%
    }

    ; change to list view
    ImageSearch, OutputVarX, OutputVarY, 0, 0, %SteamWidth%, %SteamHeight%, steam_ku_list.png
    if(OutputVarX)
    {
        Click, %OutputVarX%, %OutputVarY%
    }

    ; move mouse right 500 px
    MouseGetPos, MousePosX, MousePosY
    MousePosX += 500
    MouseMove, %MousePosX%, %MousePosY%

    Sleep, 100

    ; move mouse above all games
    ImageSearch, OutputVarX, OutputVarY, 0, 0, %SteamWidth%, %SteamHeight%, steam_ku_title.png
    MouseMove, %OutputVarX%, %OutputVarY%

    DeleteAllGamesTagsInLibary(SteamWidth, SteamHeight)

    return

^l::
    MsgBox, Reload
    Reload
    return

; 193754
; https://steamdb.info/search/?a=app&q=dead+cell
; https://steamdb.info/app/588650/info
; https://store.steampowered.com/app/588650
; <b>Genre:</b>

DeleteAllGamesTagsInLibary(SteamWidth, SteamHeight)
{
    SliderHeightBefore := -2
    SliderHeight := -1
    NeedMoveMouse := 1
    While(NeedMoveMouse or SliderHeight != SliderHeightBefore)
    {
        LSMoveMouseNextGame()
        MouseGetPos, MousePosX, MousePosY
        OpenTagsWindow(SteamWidth, SteamHeight)

        ; prevent to jump with game operated now
        Send, !{Tab}
        Sleep, 200
        Send, {Down}
        Sleep, 200
        MouseMove, %MousePosX%, %MousePosY%
        NeedMoveMouse := LSNextGameNeedToMoveMouse()
        Send, !{Tab}
        Sleep, 200

        DeleteAllTagsWhenTagsWindow()

        Sleep, 300
        MouseMove, %MousePosX%, %MousePosY%

        SliderHeightBefore := SliderHeight
        ImageSearch, , SliderHeight, 0, 0, %SteamWidth%, %SteamHeight%, steam_libary_game_tags_slider.png
    }
}

OpenTagsWindow(SteamWidth, SteamHeight)
{
    ; open tags window
; MsgBox, To open set tags window.
    Sleep, 100
    Click, right
    Sleep, 300
    ImageSearch, SetTagsPosX, SetTagsPosY, 0, 0, SteamWidth, SteamHeight, steam_libary_settag.png
    SetTagsPosX += 5
    SetTagsPosY += 5
    Click, %SetTagsPosX%, %SetTagsPosY%
    Sleep, 300
}

DeleteAllTagsWhenTagsWindow()
{
    WinGetActiveStats, Title, Width, Height, X, Y
    ImageSearch, SliderDownPosX, SliderDownPosY, 0, 0, %Width%, %Height%, steam_libary_game_tags_slider_down.png
    if(not SliderDownPosX)
    {
        MsgBox, SliderDown not Found!
    }
    SliderDownPosX += 5
    SliderDownPosY += 5

; MsgBox, To delete all tags.
    While(1)
    {
        Sleep, 200
        ImageSearch, TogglePosX, TogglePosY, 0, 0, %Width%, %Height%, steam_libary_game_tags_toggle.png
        While(TogglePosX)
        {
            Click, %TogglePosX%, %TogglePosY%
            Sleep, 100
            ImageSearch, TogglePosX, TogglePosY, 0, 0, %Width%, %Height%, steam_libary_game_tags_toggle.png
        }
        if(not CanContinueHandleTagsWindow(Width, Height))
        {
            break
        }

        Click, %SliderDownPosX%, %SliderDownPosY%
    }
    ImageSearch, ConfirmPosX, ConfirmPosY, 0, 0, %Width%, %Height%, steam_libary_game_tags_confirm.png
    if(ConfirmPosX)
    {
        Click, %ConfirmPosX%, %ConfirmPosY%
    }
    else
    {
        MsgBox, Confirm button not Found!
    }
    return
}

CanContinueHandleTagsWindow(Width, Height)
{
    Sleep, 300
    ImageSearch, DownPosX, DownPosY, 0, 0, Width, Height, steam_libary_game_tags_noslider.png
    if(DownPosX)
    {
        return ""
    }
    ImageSearch, DownPosX, DownPosY, 0, 0, Width, Height, steam_libary_game_tags_slider.png
    if(DownPosY)
    {
        return 1
    }
    ; MsgBox, No more tags.
    return ""
}
LSNextGameNeedToMoveMouse()
{
    SelectedColor = 0x543719

    MouseGetPos, PosX, PosY
    PixelGetColor, MouseColor, %PosX%, %PosY%

    if(MouseColor != SelectedColor)
    {
        return 1
    }
    return ""
}
LSMoveMouseNextGame()
{
    SelectedColor = 0x543719
    MovePixelsEachTime := 10
    WinGetPos, , , Width, Height

    MouseGetPos, PosX, PosY
    PixelGetColor, MouseColor, %PosX%, %PosY%
    IfFind := 1

    While(MouseColor != SelectedColor)
    {
        PosY += MovePixelsEachTime
        if(PosY >= Height)
        {
            IfFind := ""
            break
        }
        PixelGetColor, MouseColor, %PosX%, %PosY%
    }
    MouseMove, %PosX%, %PosY%
    IfFind := PoxY

    return IfFind
}

LSChangeToWindow(winName)
{
    uid := WinExist(winName)
    if(uid)
    {
        WinActivate, %winName%
        ; MsgBox, active
    }
    ; MsgBox, over
    return uid
}

LSRequestSteamStorePage(appid)
{
    ; url = https://steamdb.info/search/?a=app&q=dead+cell
    ; url = https://steamdb.info/app/588650/info

    url = https://store.steampowered.com/app/%appid%
    ; MsgBox, %url%
    res := LSRequest(url)
    ; file := FileOpen("res.txt", "w", "UTF-8")
    ; file.Write(res)
    ; MsgBox, over
    return res
}

LSRequest(url)
{
    ; Example: Download text to a variable:
    whr := ComObjCreate("WinHttp.WinHttpRequest.5.1")
    whr.Open("GET", url, true)
    whr.Send()
    ; Using 'true' above and the call below allows the script to remain responsive.
    whr.WaitForResponse()
    version := whr.ResponseText
    ; MsgBox % version
    return version
}
