^p::
    MsgBox, 4, , Reload?
    IfMsgBox, Yes
    {
        Reload
    }
    return

^u::
    SetTitleMatchMode, 2
    if WinExist("Mozilla Firefox")
    {
        WinActivate, Mozilla Firefox
        WinWaitActive, Mozilla Firefox
        Send, !h
        Sleep, 300
        Send, a
    }
    return